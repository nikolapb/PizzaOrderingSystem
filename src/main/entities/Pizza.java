package main.entities;

import java.io.Serializable;

public class Pizza implements Serializable {

    private PizzaSize size;
    private PizzaType type;
    private boolean catchup;
    private boolean mayonnaise;
    private boolean oregano;

    public Pizza(PizzaType type, PizzaSize size,  boolean catchup, boolean mayonnaise, boolean oregano) {
        this.type = type;
        this.size = size;
        this.catchup = catchup;
        this.mayonnaise = mayonnaise;
        this.oregano = oregano;
    }

    public PizzaSize getSize() {
        return size;
    }

    public void setSize(PizzaSize size) {
        this.size = size;
    }

    public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public boolean isCatchup() {
        return catchup;
    }

    public void setCatchup(boolean catchup) {
        this.catchup = catchup;
    }

    public boolean isMayonnaise() {
        return mayonnaise;
    }

    public void setMayonnaise(boolean mayonnaise) {
        this.mayonnaise = mayonnaise;
    }

    public boolean isOregano() {
        return oregano;
    }

    public void setOregano(boolean oregano) {
        this.oregano = oregano;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "type=" + type +
                ", size=" + size +
                ", catchup=" + catchup +
                ", mayonnaise=" + mayonnaise +
                ", oregano=" + oregano +
                '}'+"\n";
    }
}
