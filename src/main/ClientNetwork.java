package main;

import main.entities.Pizza;
import main.entities.PizzaOrder;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientNetwork {
    public static final int TCP_PORT = 9000;

    private ObjectInputStream in;
    private Socket socket = null;
    private ObjectOutputStream out;

    public ClientNetwork() {


        try {

            InetAddress addr = InetAddress.getByName("127.0.0.1");
            socket = new Socket(addr, TCP_PORT);

            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void run(PizzaOrder pizza) {
        try {

            System.out.println("[Client]: Started...");

            System.out.println("[Client]: Sending pizza order...");
            PizzaOrder pizzaOrder = pizza;
            out.writeObject(pizzaOrder);
            System.out.println("[Client]: Pizza order placed.");
            System.out.println("[Client]: Pizza ordered: " + pizzaOrder);
            in.close();
            out.close();
            socket.close();
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}