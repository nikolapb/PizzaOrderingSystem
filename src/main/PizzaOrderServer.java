package main;

import java.net.ServerSocket;
import java.net.Socket;

public class PizzaOrderServer {
    public static final int TCP_PORT = 9000;

    public static void main(String[] args) {
        try {
            int clientCounter = 0;
            ServerSocket ss = new ServerSocket(TCP_PORT);
            System.out.println("Server running...");
            ServerGUI serverGUI = new ServerGUI();
            while (true) {
                Socket socket = ss.accept();
                System.out.println("Client accepted:" + (++clientCounter));
                PizzaServerNetwork st = new PizzaServerNetwork(socket, clientCounter,serverGUI);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
