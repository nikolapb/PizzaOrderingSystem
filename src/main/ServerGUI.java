package main;


import main.entities.PizzaOrder;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;


public class ServerGUI {

    private JFrame frame = new JFrame("Server Gui");
    private JPanel porudzbine = new JPanel();
    private JPanel isporuceno = new JPanel();
    private JLabel orderLabel = new JLabel(" ");
    private ArrayList<JLabel> arrayList = new ArrayList<>();
    private int count = 0;

    public ServerGUI() {


        frame.setSize(800, 1000);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);


        JLabel porudzbineLabel = new JLabel("PORUDZBINE");
        porudzbineLabel.setSize(100, 50);
        porudzbineLabel.setLocation(120, 0);
        frame.add(porudzbineLabel);

        JLabel isporucenoLabel = new JLabel("ISPORUCENO");
        isporucenoLabel.setSize(100, 50);
        isporucenoLabel.setLocation(550, 0);
        frame.add(isporucenoLabel);

        porudzbine.setBackground(new Color(236, 223, 223));
        porudzbine.setSize(390, 945);
        porudzbine.setLocation(5, 50);


        isporuceno.setBackground(new Color(225, 221, 221));
        isporuceno.setSize(390, 945);
        isporuceno.setLocation(410, 50);


        frame.add(porudzbine);
        frame.add(isporuceno);
        frame.setVisible(true);
    }


    public void run(PizzaOrder pizzaOrder) {

        count++;
       Thread t =  new Thread(new Runnable() {
            @Override
            public void run() {

                generateOrderForGui(pizzaOrder, count);

            }
        });

       t.start();
//        frame.setVisible(true);
    }

    private void generateOrderForGui(PizzaOrder pizzaOrder, int count) {

        Random randomTime = new Random();
        int minTime = 20;
        int maxTime = 60;

        int time = randomTime.nextInt(maxTime - minTime) + minTime;

//        System.out.println("new pizza ordered");
//        System.out.println("TIME : " + time);

       JLabel orderLabel = new JLabel();
        orderLabel.setSize(350, 300);

        String ingredient = checkIngredients(pizzaOrder);

        String order = "<html><p>" + pizzaOrder.getPizza().getType() + ", " + pizzaOrder.getPizza().getSize() + ", " + ingredient + ", <br>" +
                pizzaOrder.getPayment() + ", " + pizzaOrder.getAddress() + ", <br>" + pizzaOrder.getPhoneNumber() + ", " +
                pizzaOrder.getNote();

        orderLabel.setText(order + " | " + time + " min " +"<br>-------------------------------------" + "</p></html>");

        arrayList.add(orderLabel);

        porudzbine.add(orderLabel);
        frame.setVisible(true);

        while (time > 0) {

            try {
                Thread.sleep(1000);
                time -= 1;
                orderLabel.setText(order + " | " + time + " min " + "<br>-------------------------------------" + "</p></html>");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        porudzbine.remove(orderLabel);
        porudzbine.validate();
        porudzbine.repaint();

        orderLabel.setText(order + "<html><br>-------------------------------------</html>");
        isporuceno.add(orderLabel);

        frame.setVisible(true);


    }

    public String checkIngredients(PizzaOrder pizzaOrder) {

        String ingredient = " ";

        if (pizzaOrder.getPizza().isCatchup()) {
            ingredient += "kecap ";
        }

        if (pizzaOrder.getPizza().isMayonnaise()) {
            ingredient += " majonez";
        }

        if (pizzaOrder.getPizza().isOregano()) {
            ingredient += " origano";
        }
        if (!pizzaOrder.getPizza().isCatchup() &&
                !pizzaOrder.getPizza().isMayonnaise() &&
                !pizzaOrder.getPizza().isOregano()) {
            ingredient = "bez zacina";
        }


        return ingredient;


    }


}
