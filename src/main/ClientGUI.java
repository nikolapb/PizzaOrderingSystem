package main;

import main.entities.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientGUI extends JFrame{


    private JLabel pizzaTextArea;
    private JComboBox<PizzaType> pizzaMenu;
    private JLabel sizeTextArea;
    private JComboBox<PizzaSize> sizeMenu;
    private JLabel dodaciLabel;
    private JCheckBox kecapCheckBox;
    private JCheckBox majonezCheckBox;
    private JCheckBox origanoCheckBox;
    private JLabel placanjeLabel;
    private JComboBox<Payment> placanjeMenu;
    private JLabel adresaLabel;
    private JTextField adresaTextFiled;
    private JLabel brojLabel;
    private JTextField brojTelefonaTextField;
    private JLabel napomenaLabel;
    private JTextField napomenaTextField;
    private JButton poruciButton;



    public static void main(String[] args) {

        new ClientGUI().guiWindow();

    }


    public void guiWindow(){

        setSize(300,550);
        setTitle("Pizza Order");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        pizzaTextArea = new JLabel("Pizza: ");
        pizzaTextArea.setLocation(5,0);
        pizzaTextArea.setSize(100,50);
        getContentPane().add(pizzaTextArea);

        pizzaMenu = new JComboBox<PizzaType>(PizzaType.values());
        pizzaMenu.setLocation(75,12);
        pizzaMenu.setSize(150,25);
        getContentPane().add(pizzaMenu);

        sizeTextArea = new JLabel("Velicina: ");
        sizeTextArea.setLocation(5,35);
        sizeTextArea.setSize(100,50);
        getContentPane().add(sizeTextArea);

        sizeMenu = new JComboBox<PizzaSize>(PizzaSize.values());
        sizeMenu.setLocation(75,47);
        sizeMenu.setSize(150,25);
        getContentPane().add(sizeMenu);

        dodaciLabel = new JLabel("Dodaci: ");
        dodaciLabel.setLocation(5,73);
        dodaciLabel.setSize(100,50);
        getContentPane().add(dodaciLabel);

        kecapCheckBox = new JCheckBox("kecap");
        kecapCheckBox.setLocation(5,110);
        kecapCheckBox.setSize(100,25);
        getContentPane().add(kecapCheckBox);

        majonezCheckBox = new JCheckBox("majonez");
        majonezCheckBox.setLocation(103,110);
        majonezCheckBox.setSize(100,25);
        getContentPane().add(majonezCheckBox);

        origanoCheckBox = new JCheckBox("origano");
        origanoCheckBox.setLocation(200,110);
        origanoCheckBox.setSize(100,25);
        getContentPane().add(origanoCheckBox);

        placanjeLabel = new JLabel("Placanje: ");
        placanjeLabel.setLocation(5,135);
        placanjeLabel.setSize(150,50);
        getContentPane().add(placanjeLabel);

        placanjeMenu = new JComboBox<Payment>(Payment.values());
        placanjeMenu.setLocation(75,150);
        placanjeMenu.setSize(150,25);
        getContentPane().add(placanjeMenu);

        adresaLabel = new JLabel("Adresa: ");
        adresaLabel.setLocation(5,175);
        adresaLabel.setSize(100,50);
        getContentPane().add(adresaLabel);

        adresaTextFiled = new JTextField();
        adresaTextFiled.setLocation(5,215);
        adresaTextFiled.setSize(225,30);
        getContentPane().add(adresaTextFiled);

        brojLabel = new JLabel("Telefon: ");
        brojLabel.setLocation(5,250);
        brojLabel.setSize(100,50);
        getContentPane().add(brojLabel);

        brojTelefonaTextField = new JTextField();
        brojTelefonaTextField.setLocation(5,290);
        brojTelefonaTextField.setSize(225,30);
        getContentPane().add(brojTelefonaTextField);

        napomenaLabel = new JLabel("Napomena: ");
        napomenaLabel.setLocation(5,325);
        napomenaLabel.setSize(100,50);
        getContentPane().add(napomenaLabel);

        napomenaTextField = new JTextField();
        napomenaTextField.setLocation(5,365);
        napomenaTextField.setSize(225,75);
        getContentPane().add(napomenaTextField);

        poruciButton = new JButton("PORUCI");
        poruciButton.setLocation(5,465);
        poruciButton.setSize(100,30);
        getContentPane().add(poruciButton);


        setVisible(true);


        poruciButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Pizza pizzaIngredients = new Pizza((PizzaType)pizzaMenu.getSelectedItem(),(PizzaSize)sizeMenu.getSelectedItem(),
                                        kecapCheckBox.isSelected(),majonezCheckBox.isSelected(),origanoCheckBox.isSelected());

                PizzaOrder pizza = new PizzaOrder(pizzaIngredients,(Payment)placanjeMenu.getSelectedItem(),adresaTextFiled.getText(),
                                                    brojTelefonaTextField.getText(),napomenaTextField.getText());


                ClientNetwork clientNetwork = new ClientNetwork();
                clientNetwork.run(pizza);
            }
        });



    }


}
