package main.entities;

import java.io.Serializable;

public class PizzaOrder implements Serializable {

    private Pizza pizza;
    private Payment payment;
    private String address;
    private String phoneNumber;
    private String note;

    public PizzaOrder(Pizza pizza) {
        this.pizza = pizza;
    }


    public PizzaOrder(Pizza pizza, Payment payment, String address, String phoneNumber, String note) {
        this.pizza = pizza;
        this.payment = payment;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.note = note;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "PizzaOrder{" +
                "pizza=" + pizza +
                " payment=" + payment +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
