package main;


import main.entities.PizzaOrder;


import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class PizzaServerNetwork extends Thread {
    private PizzaOrder pizza;
    private ServerGUI serverGUI;
    private Socket socket;
    private int value;
    private ObjectInputStream in;
    private ObjectOutputStream out;

    public PizzaServerNetwork(Socket socket, int value, ServerGUI serverGUI) {
        this.socket = socket;
        this.value = value;
        this.serverGUI = serverGUI;
        try {

            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        start();
    }

    public void run() {
        try {

            pizza = (PizzaOrder) in.readObject();

//            System.out.println("Reading stream...");

            serverGUI.run(pizza);

//            System.out.println("Pizza ordered: " + pizza);


            in.close();
            out.close();
            socket.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}